#take input
#if date
#store date and description
#elif day 

#echo today
#grep(date + "%d/%m/%y")
#echo recurring
#grep $(date + "%d")
#grep $(date + "%D")

#addagenda
#take input
#format and store .agenda

#take input
read -p "What date? " date
read -p "What event? " event

if [ $date ]
then
	#format and store into .agenda
	echo "$date:$event" >> .agenda
fi

#checkagenda
#get current date
#check if any .agenda match current date
#output matches 

#checkagenda
#current date
todate=$(date +"%x")
dayoftheweek=$(date +"%a")
dayofthemonth=$(date +"%d")

#check if any in .agenda match current date
#output matches 
echo ""
echo "Today's events"

#grep (grab a regular expression and print it)
grep $todate .agenda

echo ""
echo "Weekly recurring events"
grep "$dayoftheweek:" .agenda

echo ""
echo "Monthly recurring events"
grep "$dayofmonth:" .agenda
