#ask for user input to remind 
#store title and description
#if user types title 
#return description reminder 

#!/usr/bin/bash
#ask user reminder input
echo "Remind me"
echo "Subject:"
read subject
echo "Description:"
read note

#save in .reminder file 
echo "$subject: $note" >> .reminder

echo "" 
#ask if they need to see reminders
echo "Do you need to see your list of your reminders? [Yes, No, Filter]"
read answer
#if yes then list them
if [ $answer = "Yes" ]
then 
	echo "List of reminders: " 
	cat -v .reminder
#else if no then display comment
elif [ $answer = "No" ]
then 
	echo "OKAY then, don't blame me if you don't remember things!"
#list by filter
elif [ $answer = "Filter" ]
then 
	echo "Enter what you are looking for?"
	read filter
	cat -v .reminder | grep $filter
fi
